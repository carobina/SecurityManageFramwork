# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : logfun.py

from .. import models


def createLog(data):
    """
    data={
    'type':'',
    'user':'',
    'action':'',
    'status':'',
    }
    """
    models.Log.objects.create(
        type=data["type"]
        , user=data["user"]
        , ip=data['ip']
        , action=data["action"]
        , status=data['status']
    )
    return True