# -*- coding: utf-8 -*-
# @Time    : 2020/6/17
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : initdata.py
import django, os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'SemfPro.settings')
django.setup()
from RBAC.models import Permission, Menu, Role

from Asset import models as assetmodels
from Vuln import models as vulnmodels
from Workflow import models as workflowmodels
from Task import models as taskmodels
from ApiManage import models as apimodels


def initmenu():
    menu_list = [
        {'name': '资产管理', 'is_show': True, 'key': 'asset', 'icon': "el-icon-money", 'path': '', 'component': '',
         'parent': ''},
        {'name': '业务线', 'is_show': True, 'key': 'assetroot', 'icon': '', 'path': '/asset/root',
        'component':'/asset/root/', 'parent': 'asset'},
        {'name': '资产列表', 'is_show': True, 'key': 'assetlist', 'icon': '', 'path': '/asset/list',
         'component': '/asset/list/', 'parent': 'asset'},
        {'name': '资产详情', 'is_show': False, 'key': 'assetdetial', 'icon': '', 'path': '/asset/detial/:id',
        'component':'/asset/detial/','parent': 'asset'},
        {'name': '漏洞管理', 'is_show': True, 'key': 'vuln', 'icon': "el-icon-_bug", 'path': '', 'component': '',
         'parent': ''},
        {'name': '漏洞列表', 'is_show': True, 'key': 'vulnlist', 'icon': '', 'path': '/vuln/list',
        'component':'/vuln/list/', 'parent': 'vuln'},
        {'name': '待修复', 'is_show': True, 'key': 'vulnfix', 'icon': '', 'path': '/vuln/vulnfix',
         'component': '/vuln/workflow/fix', 'parent': 'vuln'},
        {'name': '漏洞审核', 'is_show': True, 'key': 'vulnworkflow', 'icon': '', 'path': '/vuln/workflow',
         'component': '/vuln/workflow/', 'parent': 'vuln'},
        {'name': '漏洞复查', 'is_show': True, 'key': 'vulncheck', 'icon': '', 'path': '/vuln/vulncheck',
         'component': '/vuln/workflow/vulncheck', 'parent': 'vuln'},
        {'name': '任务管理', 'is_show': True, 'key': 'task', 'icon': "el-icon-tickets", 'path': '', 'component': '',
         'parent': ''},
        {'name': '任务列表', 'is_show': True, 'key': 'tasklist', 'icon': '', 'path': '/task/list',
        'component':'/task/list', 'parent': 'task'},
        {'name': '处理中', 'is_show': True, 'key': 'taskdeal', 'icon': '', 'path': '/task/deal',
         'component': '/task/workflow/deal', 'parent': 'task'},
        {'name': '待办任务', 'is_show': True, 'key': 'taskworkflow', 'icon': '', 'path': '/task/workflow',
         'component': '/task/workflow/', 'parent': 'task'},
        {'name': '安全工具', 'is_show': True, 'key': 'tools', 'icon': "el-icon-paperclip", 'path': '', 'component': '',
         'parent': ''},
        {'name': '回显记录', 'is_show': True, 'key': 'echo', 'icon': '', 'path': '/tools/echo',
        'component':'/tools/echo/', 'parent': 'tools'},
        {'name': '接口测试', 'is_show': True, 'key': 'post', 'icon': '', 'path': '/tools/post',
        'component':'/tools/post/', 'parent': 'tools'},
        {'name': '系统管理', 'is_show': True, 'key': 'system', 'icon': "el-icon-setting", 'path': '', 'component': '',
         'parent': ''},
        # {'name': '用户管理', 'is_show': True, 'key': 'userlist', 'icon': '', 'path': '/system/user','component':'/system/user', 'parent': 'system'},
        {'name': '基础信息', 'is_show': True, 'key': 'base', 'icon': '', 'path': '/system/base',
        'component':'/system/base', 'parent': 'system'},
    ]

    for item in menu_list:
        menu_get = Menu.objects.get_or_create(key=item['key'])
        menu_get = menu_get[0]
        menu_get.jump = item['path']
        menu_get.component = item['component']
        menu_get.icon = item['icon']
        menu_get.is_show = item['is_show']
        menu_get.name = item['name']
        menu_get.parent = Menu.objects.filter(key=item['parent']).first()
        menu_get.save()
    print('initmenu OK')


def initPermission():
    # 权限对应的菜单均为第一级菜单，情确保访问该菜单相关页面url均已授权至菜单
    permission_list = [
        # {'name': '用户管理', 'url': '/user/', 'menu': 'set'},
        # {'name': '通告管理', 'url': '/announce/manage/', 'menu': 'set'},
    ]
    for item in permission_list:
        permission_get = Permission.objects.get_or_create(
            name=item['name'],
            url=item['url'],
        )
        permission_get = permission_get[0]
        permission_get.menu = Menu.objects.filter(key=item['menu']).first()
        permission_get.save()
    print('initPermission OK')


def initRole():
    role_list = [
        # {'name': '业务账号', 'description': '系统基本使用权限', 'menu': ['assetlist', 'vulnmanage']},
    ]
    for item in role_list:
        role_get = Role.objects.get_or_create(name=item['name'])
        role_get = role_get[0]
        role_get.description = item['description']
        for menu_get in Menu.objects.filter(key__in=item['menu']):
            role_get.menu.add(menu_get)
        role_get.save()
    print('initRole ok')


def initassettype():
    type_list = [
        {'name': '服务器', 'is_root': False, 'description': '具象化资产', 'parent': ''},
        {'name': 'WEB应用', 'is_root': False, 'description': 'WEB服务', 'parent': ''},
        {'name': '其他', 'is_root': False, 'description': '其他资产，未加定义', 'parent': ''},
    ]
    for item in type_list:
        item_get = assetmodels.Type.objects.get_or_create(name=item['name'])
        item_get = item_get[0]
        item_get.is_root = item['is_root']
        item_get.description = item['description']
        item_get.parent = assetmodels.Type.objects.filter(name=item['parent']).first()
        item_get.save()
    print('initassettype ok')


def initassettypeinfo():
    type_list = [
        {'key': 'web', 'name': '网站属性', 'type_connect': ['WEB应用', ]},
        {'key': 'plugin', 'name': '组件属性', 'type_connect': ['WEB应用']},
        {'key': 'os', 'name': '操作系统属性', 'type_connect': ['服务器', ]},
        {'key': 'port', 'name': '端口属性', 'type_connect': ['服务器', ]},
        {'key': 'tag', 'name': '端口属性', 'type_connect': ['服务器', 'WEB应用']},
        {'key': 'vuln', 'name': '端口属性', 'type_connect': ['服务器', 'WEB应用']},
    ]
    for item in type_list:
        item_get = assetmodels.TypeInfo.objects.get_or_create(name=item['name'], key=item['key'])
        item_get = item_get[0]
        for type_get in assetmodels.Type.objects.filter(name__in=item['type_connect']):
            item_get.type_connect.add(type_get)
        item_get.save()
    print('initassettypeinfo ok')


def initLanguageType():
    type_list = [
        {'name': 'Other'},
        {'name': 'C/C++'},
        {'name': 'C#'},
        {'name': 'Ruby'},
        {'name': 'JAVA'},
        {'name': 'ASP.NET'},
        {'name': 'JSP'},
        {'name': 'PHP'},
        {'name': 'Perl'},
        {'name': 'Python'},
        {'name': 'VB.NET'},
    ]
    for item in type_list:
        assetmodels.LanguageType.objects.get_or_create(name=item['name'])
    print('initLanguageType ok')


def initVulnLevel():
    type_list = [
        {'name': '信息'},
        {'name': '低危'},
        {'name': '中危'},
        {'name': '高危'},
        {'name': '紧急'},
    ]
    for item in type_list:
        vulnmodels.LEVEL.objects.get_or_create(name=item['name'])
    print('initVulnLevel ok')


def initVulnType():
    type_list = [
        {'name': 'SQL注入'},
        {'name': '越权漏洞'},
        {'name': 'XSS漏洞'},
        {'name': '配置错误'},
        {'name': '敏感信息泄露'},
        {'name': 'SSRF漏洞'},
        {'name': 'CSRF漏洞'},
        {'name': '未授权访问'},
        {'name': '逻辑漏洞'},
        {'name': '其他安全风险'},
    ]
    for item in type_list:
        vulnmodels.Type.objects.get_or_create(name=item['name'])
    print('initVulnType ok')


def initVulnSource():
    type_list = [
        {'name': 'X-RAY', 'key': 'xray'},
        {'name': 'Poc检测', 'key': 'poc'},
        {'name': '人工检测', 'key': 'user'},
        {'name': '外部上报', 'key': 'public'},
        {'name': '外界同步', 'key': 'sync'},
    ]
    for item in type_list:
        vulnmodels.Source.objects.get_or_create(name=item['name'], key=item['key'])
    print('initVulnType ok')


# 需执行两次
def initWorkFlow():
    list_flow = [
        # 漏洞审核 vuln 漏洞处理 vuln_fix  漏洞复查 vuln_check
        # 详情页：漏洞详情（下一版凑成组件） + 流程单日志（组件）
        {'name': 'vuln_flow_A', 'key': 'vuln', 'is_check': False, 'status': '待确认', 'parent': 'vuln_check'},
        {'name': 'vuln_flow_B', 'key': 'vuln_fix', 'is_check': True, 'status': '待修复', 'parent': 'vuln'},
        {'name': 'vuln_flow_C', 'key': 'vuln_ignore', 'is_check': False, 'status': '已忽略', 'parent': 'vuln'},
        {'name': 'vuln_flow_D', 'key': 'vuln_check', 'is_check': True, 'status': '待复查', 'parent': 'vuln_fix'},
        {'name': 'vuln_flow_E', 'key': 'vuln_accept', 'is_check': False, 'status': '非风险', 'parent': 'vuln_fix'},
        {'name': 'vuln_flow_F', 'key': 'vuln_finish', 'is_check': True, 'status': '已修复', 'parent': 'vuln_check'},
        # 代办任务 task  XX任务 task_deal
        # 详情页：基础属性，下面该任务下的漏洞和资产列表
        {'name': 'task_flow_A', 'key': 'task', 'is_check': False, 'status': '待处理', 'parent': 'task_deal'},
        {'name': 'task_flow_B', 'key': 'task_deal', 'is_check': True, 'status': '处理中', 'parent': 'task'},
        {'name': 'task_flow_C', 'key': 'task_cancel', 'is_check': False, 'status': '已结束', 'parent': 'task'},
        {'name': 'task_flow_D', 'key': 'task_finish', 'is_check': True, 'status': '已完成', 'parent': 'task_deal'},

    ]
    for item in list_flow:
        workflow_get = workflowmodels.Workflow.objects.get_or_create(
            name=item.get('name'),
            key=item.get('key'),
            is_check=item.get('is_check'),
            status=item.get('status'),
        )
        workflow_get = workflow_get[0]
        parent_get = workflowmodels.Workflow.objects.filter(key=item.get('parent')).first()
        if parent_get:
            workflow_get.parent = parent_get
            workflow_get.save()
    print('initWorkFlow OK')


def initTaskType():
    list_type = [
        {'name': '自动扫描', 'key': 'scan'},
        {'name': '人工评测', 'key': 'manual'},
        {'name': '周期检查', 'key': 'cycle'},
    ]
    for item in list_type:
        taskmodels.Type.objects.get_or_create(
            name=item.get('name'),
            key=item.get('key')
        )
    print('initTaskType OK')


def initApiType():
    list_type = [
        {'name': '漏洞', 'key': 'vuln'},
        {'name': '资产', 'key': 'asset'},
        {'name': '人员', 'key': 'person'},
        {'name': '部门', 'key': 'department'},
        {'name': '终端', 'key': 'client'},

    ]
    for item in list_type:
        apimodels.Type.objects.get_or_create(
            name=item.get('name'),
            key=item.get('key')
        )
    print('initApiType OK')


if __name__ == "__main__":
    initmenu()
    initPermission()
    initRole()
    initassettype()
    initassettypeinfo()
    initLanguageType()
    initVulnLevel()
    initVulnSource()
    initVulnType()
    initWorkFlow()
    initWorkFlow()
    initTaskType()
    initApiType()
