# -*- coding: utf-8 -*-
# @Time    : 2020/5/18
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : forms.py


from django.forms import ModelForm
from . import models
from django import forms


class VulnForm(ModelForm):
    asset_get = forms.CharField()

    class Meta:
        model = models.Vuln
        fields = ('name', 'type', 'level', 'introduce', 'info', 'fix', 'asset_get')