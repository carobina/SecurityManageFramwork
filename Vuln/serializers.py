# -*- coding: utf-8 -*-
# @Time    : 2020/5/18
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py

from rest_framework import serializers
from . import models
from Workflow.models import Tickets


class VulnSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField('get_status')
    asset = serializers.SerializerMethodField('get_asset')
    type = serializers.SerializerMethodField('get_type')
    level = serializers.SerializerMethodField('get_level')
    source = serializers.SerializerMethodField('get_source')
    manage = serializers.SerializerMethodField('get_manage')
    ticket_id = serializers.SerializerMethodField('get_ticket_id')
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Vuln
        fields = ('id', 'name', 'asset', 'type', 'status', 'level', 'source', 'ticket_id', 'manage', 'create_time', 'update_time')

    def get_ticket_id(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='vuln', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.id
        return None

    def get_status(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='vuln', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.workflow.status
        return None

    def get_source(self, obj):
        try:
            return {'id': obj.source.id, 'name': obj.source.name}
        except:
            return None

    def get_level(self, obj):
        try:
            return {'id': obj.level.id, 'name': obj.level.name}
        except:
            return None

    def get_type(self, obj):
        try:
            return {'id': obj.type.id, 'name': obj.type.name}
        except:
            return None

    def get_asset(self, obj):
        try:
            return {'id': obj.asset.id, 'name': obj.asset.key}
        except:
            return None

    def get_manage(self, obj):
        try:
            return {'id': obj.asset.manage.id, 'name': obj.asset.manage.name}
        except:
            return None


class AdvanceVulnSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vuln
        fields = ('id', 'name', 'type', 'level', 'source', 'create_time', 'update_time')
        depth = 1


class AdvanceVulnDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Vuln
        fields = ('id', 'name', 'type', 'level', 'source', 'introduce', 'fix', 'create_time', 'update_time')
        depth = 1


class TypeSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = ('id', 'name')


class LEVELSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LEVEL
        fields = ('id', 'name')


class VulnDetailsSerializer(serializers.ModelSerializer):
    asset = serializers.SerializerMethodField('get_asset')
    type = serializers.SerializerMethodField('get_type')
    level = serializers.SerializerMethodField('get_level')
    source = serializers.SerializerMethodField('get_source')
    user = serializers.SerializerMethodField('get_user')
    task = serializers.SerializerMethodField('get_task')
    ticket_id = serializers.SerializerMethodField('get_ticket_id')
    create_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Vuln
        fields = ('id', 'name', 'asset', 'type', 'level', 'source',
                  'is_check', 'task', 'advance_vuln', 'introduce',
                  'info', 'scope', 'fix', 'status', 'user',
                  'ticket_id', 'create_time', 'update_time')

    def get_ticket_id(self, obj):
        ticket_obj = Tickets.objects.filter(ticket_key='vuln', ticket_id=obj.id).first()
        if ticket_obj:
            return ticket_obj.id
        return None

    def get_source(self, obj):
        try:
            return {'id': obj.source.id, 'name': obj.source.name}
        except:
            return None

    def get_level(self, obj):
        try:
            return {'id': obj.level.id, 'name': obj.level.name}
        except:
            return None

    def get_type(self, obj):
        try:
            return {'id': obj.type.id, 'name': obj.type.name}
        except:
            return None

    def get_asset(self, obj):
        try:
            return {'id': obj.asset.id, 'name': obj.asset.key}
        except:
            return None

    def get_task(self, obj):
        try:
            return {'id': obj.task.id, 'name': obj.task.name}
        except:
            return None

    def get_user(self, obj):
        try:
            return {'id': obj.asset.manage.id, 'name': obj.asset.manage.name}
        except:
            return None
