# -*- coding: utf-8 -*-
# @Time    : 2020/5/18
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import vulnviews, advancevulnviews
from .views import typeviews, levelviews, assetviews


urlpatterns = [
    path('vuln/list/', vulnviews.list_views),
    path('vuln/delete/<str:vuln_id>/', vulnviews.delete_views),
    path('vuln/create/', vulnviews.create_views),

    path('base/advancevuln/list/', advancevulnviews.list_views),
    path('base/advancevuln/delete/<str:vuln_id>/', advancevulnviews.delete_views),

    path('type/select/', typeviews.select_type_views),
    path('level/select/', levelviews.select_level_views),
    # path('asset/select/', assetviews.select_asset_views),
]
