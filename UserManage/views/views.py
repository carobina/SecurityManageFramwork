# coding:utf-8
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_protect
from django.db.models import Q
from RBAC import serializers
from .. import forms
from Base.models import PermissionManage


@api_view(['GET'])
def list_views(request):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    key = request.GET.get('key', '')
    if request.user.is_superuser:
        list_get = User.objects.filter(
            Q(username__icontains=key) |
            Q(profile__title__icontains=key)
        ).order_by('-id')
        list_count = list_get.count()
        pg = MyPageNumberPagination()
        list_page = pg.paginate_queryset(list_get, request, 'self')
        serializers_get = serializers.UserListSerializer(instance=list_page, many=True)
        data['msg'] = 'success'
        data['count'] = list_count
        data['data'] = xssfilter(serializers_get.data)
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['GET'])
def deny_views(request, user_id):
    data = {
        "code": 1,
        "msg": "",
    }
    if request.user.is_superuser:
        item_get = User.objects.filter(id=user_id, is_superuser=False).first()
        if item_get:
            if item_get.is_active:
                item_get.is_active = False
            else:
                item_get.is_active = True
            item_get.save()
            data['code'] = 0
            data['msg'] = 'success'
        else:
            data['msg'] = '所选用户不存在'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def create_views(request):
    data = {
        "code": 1,
        "msg": "",
    }
    if request.user.is_superuser:
        form = forms.UserEditForm(request.POST)
        if form.is_valid():
            user_get = User.objects.get_or_create(
                username=form.cleaned_data['username'],
            )
            if user_get[1]:
                user_get = user_get[0]
                user_get.profile.person = form.cleaned_data['person']
                user_get.profile.roles = form.cleaned_data['roles']
                user_get.is_active = True
                user_get.profile.is_resetpsd = True
                user_get.set_password(form.cleaned_data['password'])
                permissionmanage_list = PermissionManage.objects.filter(
                    id__in=form.cleaned_data['permissionmanage'].split(',')
                )
                user_get.profile.permissionmanage.add(*permissionmanage_list)
                user_get.save()
                data['code'] = 0
                data['msg'] = 'success'
            else:
                data['msg'] = '用户已存在'
        else:
            data['msg'] = '请检查输入'
    else:
        data['msg'] = '权限不足'
    return JsonResponse(data)