# -*- coding: utf-8 -*-
# @Time    : 2020/10/13
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : workflowviews.py
from django.http import JsonResponse
from rest_framework.decorators import api_view
from Base.Functions.basefun import MyPageNumberPagination, xssfilter
from .. import models ,forms
from .. import serializers
from django.views.decorators.csrf import csrf_protect
from ..Functions import ticketfun


@api_view(['GET'])
def main_list(request, workflow_key):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    # key = request.GET.get('key', '')
    list_get = ticketfun.list_workflow(user, workflow_key).order_by('-updatetime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.TicketsSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['GET'])
def ticket_list(request, ticket_key):
    data = {
        "code": 0,
        "msg": "",
        "count": '',
        "data": []
    }
    user = request.user
    # key = request.GET.get('key', '')
    list_get = ticketfun.list_ticket(user, ticket_key).order_by('-updatetime')
    list_count = list_get.count()
    pg = MyPageNumberPagination()
    list_page = pg.paginate_queryset(list_get, request, 'self')
    serializers_get = serializers.TicketsSerializer(instance=list_page, many=True)
    data['msg'] = 'success'
    data['count'] = list_count
    data['data'] = xssfilter(serializers_get.data)
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def ticket_action(request, action, ticket_id):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    item_get = ticketfun.check_user_permission(user, ticket_id)
    if action == 'pass':
        status_get = True
    else:
        status_get = False
    if item_get:
        form = forms.CheckReasonForms(request.POST)
        if form.is_valid():
            description_get = form.cleaned_data['description']
            res = ticketfun.workflow_flow(item_get, user, description_get, status_get)
            if res:
                data['code'] = 0
                data['msg'] = '操作成功'
            else:
                data['msg'] = '操作失败'
        else:
            data['msg'] = '请检查输入是否合法'
    else:
        data['msg'] = '你要干啥'
    return JsonResponse(data)


@api_view(['POST'])
@csrf_protect
def ticket_many_deny(request, action):
    data = {
        "code": 1,
        "msg": "",
        "data": []
    }
    user = request.user
    form = forms.CheckManyForms(request.POST)
    if action == 'pass':
        status_get = True
    else:
        status_get = False
    if form.is_valid():
        ticket_ids = form.cleaned_data['list'].split(',')
        description_get = form.cleaned_data['description']
        for item in ticket_ids:
            item_get = ticketfun.check_user_permission(user, item)
            if item_get:
                ticketfun.workflow_flow(item_get, user, description_get, status_get)
        data['code'] = 0
        data['msg'] = '操作成功'
    else:
        data['msg'] = '请检查输入是否合法'
    return JsonResponse(data)


@api_view(['GET'])
def ticket_details(request, ticket_id):
    data = {
        "code": 1,
        "msg": "",
        "data": {}
    }
    user = request.user
    item_get = ticketfun.check_user_permission(user, ticket_id)
    if item_get:
        data['data'] = xssfilter(ticketfun.ticket_details(item_get))
        data['code'] = 0
        data['msg'] = 'success'
    else:
        data['msg'] = '请检查权限'
    return JsonResponse(data)


