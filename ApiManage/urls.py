# -*- coding: utf-8 -*-
# @Time    : 2020/11/19
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : urls.py

from django.urls import path
from .views import views, typeviews, syncviews

urlpatterns = [
    path('synckey/list/', views.list_views),

    path('sync/create/<str:synckey>/', syncviews.create_views),

    path('type/select/', typeviews.select_views),
]