# coding:utf-8
from django.db import models
from Vuln.models import LEVEL
from django.contrib.auth.models import User


# Create your models here.

class POC(models.Model):
    name = models.CharField('插件名称', max_length=50)
    key = models.CharField('插件标识', max_length=50)
    require = models.TextField('第三方包', null=True, blank=True)
    is_check = models.BooleanField('是否审核', default=False)
    level = models.ForeignKey(LEVEL, verbose_name='漏洞等级', related_name='level_for_poc', null=True,
                              on_delete=models.SET_NULL)
    introduce = models.TextField('插件简介')
    description = models.TextField('漏洞说明')
    fix = models.TextField('修复方案')
    poc = models.TextField('POC')

    user = models.ForeignKey(User, related_name='poc_for_user', verbose_name='提交人员', null=True,
                             on_delete=models.SET_NULL)
    update_user = models.ForeignKey(User, related_name='poc_for_updateuser', verbose_name='审核人员', null=True,
                                    on_delete=models.SET_NULL)

    create_data = models.DateTimeField('创建时间', auto_now_add=True)
    update_data = models.DateTimeField('操作时间', auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'POC'
        verbose_name_plural = '漏洞插件'