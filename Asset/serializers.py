# -*- coding: utf-8 -*-
# @Time    : 2020/5/12
# @Author  : canyuan
# @Email   : gy071089@outlook.com
# @File    : serializers.py

from rest_framework import serializers
from . import models
from Vuln.models import Vuln


class GroupSerializer(serializers.ModelSerializer):
    manage = serializers.SerializerMethodField('get_manage')
    parent = serializers.SerializerMethodField('get_parent_id')
    parent_name = serializers.SerializerMethodField('get_parent_name')
    asset_count = serializers.SerializerMethodField('get_asset_count')
    vuln_count = serializers.SerializerMethodField('get_vuln_count')
    add_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Group
        fields = ('id', 'name', 'description', 'manage', 'asset_count', 'parent', 'parent_name', 'vuln_count', 'add_time', 'update_time')

    def get_asset_count(self, obj):
        return obj.asset.all().count()

    def get_vuln_count(self, obj):
        asset_list = obj.asset.all()
        vuln_count = Vuln.objects.filter(asset__in=asset_list).count()
        return vuln_count

    def get_manage(self, obj):
        try:
            return {'id': obj.manage.id, 'name':obj.manage.name}
        except:
            return {'id': None, 'name':'暂无'}

    def get_parent_id(self, obj):
        if obj.parent:
            return obj.parent.id
        else:
            return '0'

    def get_parent_name(self, obj):
        if obj.parent:
            return obj.parent.name
        else:
            return None


class GroupTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Group
        fields = ('id', 'name')


class AssetSerializer(serializers.ModelSerializer):
    manage = serializers.SerializerMethodField('get_manage')
    type = serializers.SerializerMethodField('get_type')
    vuln_count = serializers.SerializerMethodField('get_vuln_count')
    add_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    update_time = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = models.Asset
        fields = ('id', 'name', 'key', 'type', 'is_out', 'is_use', 'description', 'manage', 'vuln_count', 'add_time',
                  'update_time')

    def get_type(self, obj):
        if obj.type:
            return {"id": obj.type.id, "name": obj.type.name}
        return None

    def get_manage(self, obj):
        if obj.manage:
            return {"id": obj.manage.id, "name": obj.manage.name}
        return None

    def get_vuln_count(self, obj):
        return Vuln.objects.filter(asset=obj).count()


class TypeSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = ('id', 'name')


class AssetSelectSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Type
        fields = ('id', 'name', 'key')


class TypeInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TypeInfo
        fields = ('id', 'key', 'name')


class TagInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TagInfo
        fields = ('id', 'key', 'name')


class LanguageTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.LanguageType
        fields = ('id', 'name')


class OSinfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.OsInfo
        fields = '__all__'


class WebInfoSerializer(serializers.ModelSerializer):
    language = serializers.SerializerMethodField('get_language')

    class Meta:
        model = models.WebInfo
        fields = '__all__'

    def get_language(self, obj):
        if obj.language:
            return {"id": obj.language.id, "name": obj.language.name}
        return None


class PortInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PortInfo
        fields = '__all__'


class PluginInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PluginInfo
        fields = '__all__'
